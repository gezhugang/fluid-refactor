### Candidate Chinese Name:
* 
 葛主港
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 
1、JmsMessageBrokerSupport中包含了队列所有操作方法，针对一个类只做一件事，将队列的相关操作抽象为一个服务
2、服务可采用依赖注入的方式，注入不容的实现类，来实现对代理方式的解耦
3、本例中只针对activeMq做了demo，其他的代理方式可以，在JmsMessageConnectService实现类中实现，如需实现ibmmq，在JmsMessageConnectService的ibmmq的实现方法，对获取连接工厂的方法进行重写，然后再应用类中注入该实现类即可
4、流式api查了下是java8的特性，目前并没有涉及过，只是根据相应的资料，大体按照这种方式去写的，可能不完全符合
5、用例中保留了相关方法，用于向下兼容，请查看RecoJmsMessageBrokerSupport


- - -