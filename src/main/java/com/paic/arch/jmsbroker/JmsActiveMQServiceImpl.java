package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class JmsActiveMQServiceImpl implements JmsMessageConnectService {
	
	private static Logger log = LoggerFactory.getLogger(JmsActiveMQServiceImpl.class);
	
	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

	/*
	 * 获取队列连接
	 * @see com.paic.arch.jmsbroker.JmsMessageConnectService#getConnection(java.lang.String)
	 */
	@Override
	public Connection getConnection(String aBrokerUrl) {

        Connection connection = null;
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
            
            connection = connectionFactory.createConnection();
            connection.start();
            return connection;
        } catch (JMSException jmse) {
        	log.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } 
	}
	
	/*
	 * 设置发送消息内容
	 * @see com.paic.arch.jmsbroker.JmsMessageConnectService#setSendMessage(javax.jms.Connection, java.lang.String)
	 */
	@Override
	public Message setSendMessage(Connection aConnection, String messageToSend) throws JMSException {
		
		Session session = null;
		
		try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            return session.createTextMessage(messageToSend);
        } catch (JMSException jmse) {
        	log.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } 
		finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    log.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
            
            if (aConnection != null) {
                try {
                	aConnection.close();
                } catch (JMSException jmse) {
                    log.warn("Failed to close connection to broker at []");
                    throw new IllegalStateException(jmse);
                }
            }
        }
		
	}
	
	/*
	 * 发送消息
	 * @see com.paic.arch.jmsbroker.JmsMessageConnectService#sendMessage(javax.jms.Connection, javax.jms.Message, java.lang.String)
	 */
	@Override
	public void sendMessage (Connection aConnection, Message message, String aDestinationName) {
		
		Session session = null;
		try {
			session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            MessageProducer producer = session.createProducer(queue);
            producer.send(message);
            producer.close();
        } catch (JMSException jmse) {
            log.error("Failed to create Queue on session {}", session);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    log.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
            
            if (aConnection != null) {
                try {
                	aConnection.close();
                } catch (JMSException jmse) {
                    log.warn("Failed to close connection to broker at []");
                    throw new IllegalStateException(jmse);
                }
            }
            
        }
	}
	
	/*
	 * 从队列中获取消息
	 * @see com.paic.arch.jmsbroker.JmsMessageConnectService#receiveMessage(javax.jms.Connection, java.lang.String)
	 */
	@Override
	public String receiveMessage(Connection aConnection, String aDestinationName) throws JMSException {
		Session session = null;
		
		session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		
        Queue queue = session.createQueue(aDestinationName);
		
		MessageConsumer consumer = session.createConsumer(queue);
		Message message = consumer.receive(DEFAULT_RECEIVE_TIMEOUT);
		if (message == null) {
            throw new NoMessageReceivedException("No messages received from the broker within the %d timeout");
        }
		
		if (aConnection != null) {
            try {
            	aConnection.close();
            } catch (JMSException jmse) {
                log.warn("Failed to close connection to broker at []");
                throw new IllegalStateException(jmse);
            }
        }
		
		if (session != null) {
            try {
                session.close();
            } catch (JMSException jmse) {
                log.warn("Failed to close session {}", session);
                throw new IllegalStateException(jmse);
            }
        }
        consumer.close();
        return ((TextMessage) message).getText();
		
	}
	

}
