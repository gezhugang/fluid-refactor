package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;

public interface JmsMessageConnectService {
	
	 public Connection getConnection(String aBrokerUrl);
	 
	 public Message setSendMessage(Connection aConnection, String messageToSend) throws JMSException;
	 
	 public void sendMessage(Connection aConnection, Message message, String aDestinationName);
	 
	 public String receiveMessage(Connection aConnection,String aDestinationName) throws Exception ;
	 
	 public class NoMessageReceivedException extends RuntimeException {
	        public NoMessageReceivedException(String reason) {
	            super(reason);
	        }
	    }
	
	
}
