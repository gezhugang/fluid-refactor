package com.paic.arch.jmsbroker;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;

import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 重构JmsMessageBrokerSupport实现类
 * 
 */
public class RecoJmsMessageBrokerSupport {
	
	private String brokerUrl;
	
	Message message;
	
	public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    private BrokerService brokerService;
	
	static JmsMessageConnectService jmsMessageConnectService;
	
	private static Logger log = LoggerFactory.getLogger(RecoJmsMessageBrokerSupport.class);
	
	
	private RecoJmsMessageBrokerSupport(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
    }
	
	public static RecoJmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }
	
	public static RecoJmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        log.debug("Creating a new broker at {}", aBrokerUrl);
        RecoJmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
        broker.createEmbeddedBroker();
        broker.startEmbeddedBroker();
        return broker;
    }
	
	public final String getBrokerUrl() {
        return brokerUrl;
    }
	
	public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }
	
	private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }
	
	public static RecoJmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
		/*
		 * 原activeMq连接方式
		 */
		jmsMessageConnectService = new JmsActiveMQServiceImpl();
        return new RecoJmsMessageBrokerSupport(aBrokerUrl);
    }
	
	
	public static RecoJmsMessageBrokerSupport bindToActiveMqBrokerAt(String bokerUrl) {
		/*
		 * 此处可依赖注入，注入不同的JmsMessageConnectService实现类实现解耦
		 */
		jmsMessageConnectService = new JmsActiveMQServiceImpl();
		return new RecoJmsMessageBrokerSupport(bokerUrl);
	}
	
	/*
	 * 发送消息内容设置
	 */
	public RecoJmsMessageBrokerSupport sendTheMessage(String messageToSend) throws JMSException {
		
		Connection connection = jmsMessageConnectService.getConnection(brokerUrl);
		message = jmsMessageConnectService.setSendMessage(connection, messageToSend);
		return this;
	}
	
	/*
	 * 相队列中发送消息
	 */
	public RecoJmsMessageBrokerSupport to(String queue) {
		Connection connection = jmsMessageConnectService.getConnection(brokerUrl);
		jmsMessageConnectService.sendMessage(connection, message, queue);
		return this;
	}
	
	/*
	 * 根据队列获取其中消息
	 */
	public String waitForAMessageOn(String queue) throws Exception {
		Connection connection = jmsMessageConnectService.getConnection(brokerUrl);
		return jmsMessageConnectService.receiveMessage(connection, queue);
	}
	
	
	public final RecoJmsMessageBrokerSupport andThen() {
        return this;
    }
	
	public String retrieveASingleMessageFromTheDestination(String queue, final int aTimeout) throws Exception {
		Connection connection = jmsMessageConnectService.getConnection(brokerUrl);
		return jmsMessageConnectService.receiveMessage(connection, queue);
				
	}
	
	/*
	 * 保留的原方法，用于向下兼容
	 */
	public RecoJmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) throws JMSException {
		
		return this.sendTheMessage(aMessageToSend).to(aDestinationName);
    }
	
	public String retrieveASingleMessageFromTheDestination(String aDestinationName) throws Exception {
       
		return this.waitForAMessageOn(aDestinationName);
    }
	
	
}
